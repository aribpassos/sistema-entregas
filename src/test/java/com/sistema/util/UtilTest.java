package com.sistema.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UtilTest {
	
	private double valorLitrosExperado;
	private double valorCustoCombustivelExperado;
	
	@Before
	public void carregarValores() {
		valorLitrosExperado = 1;
		valorCustoCombustivelExperado = 2.5;
	}
	
	

	@Test
	public void testCalculaLitrosGastos() {
		assertEquals(valorLitrosExperado, Util.calculaLitrosGastos(10.0, 10.0), 0.001);
	}

	@Test
	public void testCalculaCustoCombustivel() {
		assertEquals(valorCustoCombustivelExperado, Util.calculaCustoCombustivel(1.0, 2.5), 0.001);
	}

}

package com.sistema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sistema.model.FiltroRota;
import com.sistema.model.Rota;
import com.sistema.model.RotaMenorCusto;
import com.sistema.repository.Rotas;
import com.sistema.service.RotaService;

@Controller
@RequestMapping("/rotas")
public class RotaController {

	@Autowired
	private Rotas rotas;

	@Autowired
	private RotaService rotaService;

	@GetMapping
	public ResponseEntity<List<Rota>> listaRotas() {
		List<Rota> list = rotaService.findAll();

		return new ResponseEntity<List<Rota>>(list, HttpStatus.OK);
	}

	@GetMapping("/{json}")
	public ResponseEntity<RotaMenorCusto> buscarMenorRota(@PathVariable("json") String json) {
		FiltroRota filtro = JsonToObjFiltroRota(json);
		RotaMenorCusto rotaMenorCusto = rotaService.buscarMenorRota(filtro);
		return new ResponseEntity<RotaMenorCusto>(rotaMenorCusto, HttpStatus.OK);
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@RequestBody Rota rota) {		
		this.rotas.save(rota);
		String mensagem = "atualizado com sucesso !!!";
		return new ResponseEntity<String>(mensagem,HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> salvar(@RequestBody Rota rota) {		
		this.rotas.save(rota);
		String mensagem = "salvo com sucesso !!!";
		return new ResponseEntity<String>(mensagem,HttpStatus.OK);
	}
	
	@DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> delete(@RequestBody Rota rota) {		
		this.rotas.delete(rota);
		String mensagem = "excluido com sucesso !!!";
		return new ResponseEntity<String>(mensagem,HttpStatus.OK);
	}

	public Rota JsonToObjRota(String Json) {
		Rota rota = new Rota();

		JsonParser parser = new JsonParser();
		JsonElement jsonElement = parser.parse(Json);
		JsonObject rootObject = jsonElement.getAsJsonObject();
		rota.setOrigem(rootObject.get("origem").getAsString());
		rota.setDestino(rootObject.get("destino").getAsString());
		rota.setDistancia(rootObject.get("autonomia").getAsDouble());

		return rota;
	}

	public FiltroRota JsonToObjFiltroRota(String Json) {
		FiltroRota filtroRota = new FiltroRota();

		JsonParser parser = new JsonParser();
		JsonElement jsonElement = parser.parse(Json);
		JsonObject rootObject = jsonElement.getAsJsonObject();
		filtroRota.setOrigemRota(rootObject.get("origem").getAsString());
		filtroRota.setDestinoRota(rootObject.get("destino").getAsString());
		filtroRota.setAutonomia(rootObject.get("autonomia").getAsDouble());
		filtroRota.setValorLitro(rootObject.get("valor do litro").getAsDouble());

		return filtroRota;
	}

}

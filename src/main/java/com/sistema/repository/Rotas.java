package com.sistema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sistema.model.Rota;

public interface Rotas extends JpaRepository<Rota, Long> {

	@Query("from Rota u where u.origem  = :origem order by u.distancia")
	List<Rota> findByOrigem(@Param("origem") String origem);

}

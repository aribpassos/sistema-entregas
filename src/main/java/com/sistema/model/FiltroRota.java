package com.sistema.model;

import java.io.Serializable;

public class FiltroRota implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String origemRota;
	private String destinoRota;
	private double autonomia;
	private double valorLitro;
	public String getOrigemRota() {
		return origemRota;
	}
	public void setOrigemRota(String origemRota) {
		this.origemRota = origemRota;
	}
	public String getDestinoRota() {
		return destinoRota;
	}
	public void setDestinoRota(String destinoRota) {
		this.destinoRota = destinoRota;
	}
	public double getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(double autonomia) {
		this.autonomia = autonomia;
	}
	public double getValorLitro() {
		return valorLitro;
	}
	public void setValorLitro(double valorLitro) {
		this.valorLitro = valorLitro;
	}	

}

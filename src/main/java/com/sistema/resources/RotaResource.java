package com.sistema.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sistema.model.Rota;
import com.sistema.repository.Rotas;

@RestController
@RequestMapping("/teste")
public class RotaResource {
	
	private Rotas rotas;

	@GetMapping
	public ResponseEntity<List<Rota>> listar() {
		return new ResponseEntity<List<Rota>>(HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/rotas/{id}", method = RequestMethod.GET)
//	public ResponseEntity<Rota> buscar(@PathVariable("id") Integer id) {
//	  Rota rota = cursos.get(id);
//	 
//	  if (curso == null) {
//	    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//	  }
//	 
//	  return new ResponseEntity<Curso>(curso, HttpStatus.OK);
//	}

}

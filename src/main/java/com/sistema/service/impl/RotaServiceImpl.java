package com.sistema.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.sistema.model.FiltroRota;
import com.sistema.model.Rota;
import com.sistema.model.RotaMenorCusto;
import com.sistema.repository.Rotas;
import com.sistema.service.RotaService;
import com.sistema.util.Util;

@Component
public class RotaServiceImpl implements RotaService {
	@Autowired
	private Rotas rotas;

	@Autowired
	private Gson gson;

	@Override
	public String salvarRota(Rota rota) {
		rotas.save(rota);
		return "Rota Salva co sucesso!!!";
	}

	@Override
	public RotaMenorCusto buscarMenorRota(FiltroRota filtroRota) {
		boolean calculaRota;
		double valorCusto = 0;
		double litroGastos = 0;
		String rotasValorMenorCusto = "";

		List<Rota> list = rotas.findByOrigem(filtroRota.getOrigemRota());
		Rota rota = list.get(0);

		rotasValorMenorCusto = rota.getOrigem() + " ";
		litroGastos = Util.calculaLitrosGastos(rota.getDistancia(), filtroRota.getAutonomia());
		valorCusto += Util.calculaCustoCombustivel(litroGastos, filtroRota.getValorLitro());
		calculaRota = true;

		while (calculaRota) {
			list = rotas.findByOrigem(rota.getDestino());
			rota = list.get(0);
			
			rotasValorMenorCusto += rota.getOrigem() + " ";

			if (list.size() == 0 || rota.getDestino().trim().equals(filtroRota.getDestinoRota().trim())) {
				calculaRota = false;
				rotasValorMenorCusto += rota.getDestino();
			}
			
			litroGastos = Util.calculaLitrosGastos(rota.getDistancia(), filtroRota.getAutonomia());
			valorCusto += Util.calculaCustoCombustivel(litroGastos, filtroRota.getValorLitro());

		}

		RotaMenorCusto rotaMenorCusto = new RotaMenorCusto();
		rotaMenorCusto.setRota(rotasValorMenorCusto);
		rotaMenorCusto.setCusto(valorCusto);

		return rotaMenorCusto;
	}

	@Override
	public List<Rota> findAll() {
		return rotas.findAll();
	}

}

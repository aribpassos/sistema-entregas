package com.sistema.service;

import java.util.List;

import com.sistema.model.FiltroRota;
import com.sistema.model.Rota;
import com.sistema.model.RotaMenorCusto;

public interface RotaService {
	
	public String salvarRota(Rota rota);
	
	public List<Rota> findAll();

	public RotaMenorCusto buscarMenorRota(FiltroRota filtroRota);	

}

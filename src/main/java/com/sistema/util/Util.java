package com.sistema.util;

public class Util {
	
	public static double calculaLitrosGastos(double distancia, double autonomia) {
		return  distancia / autonomia;
	}
	
	public static double calculaCustoCombustivel(double qtdListros, double valorLitro) {
		return  qtdListros * valorLitro;
	}
}

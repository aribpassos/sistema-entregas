 O banco de dados incluido faz uma carga de alguns dados no script import.sql se deseja incluir mais 
dados para teste devera usar o mesmo

requisição via GET
url base = localhost/rotas
**url sem parametros = retorna todos registros de rotas cadastrados em forma de json

requisição via GET
url base = localhost/rotas
parametro json = { "origem":"A", "destino":"D", "autonomia":"10", "valor do litro":"2.50" }
função: traz o menor custo por rota
retorno exemplo: {"rota":"A B D","custo":6.25}

requisição via POST (inserir informações de rotas)
url base = localhost/rotas
Json para inclusao = { "origem":"A", "destino":"D", "distancia":"30" }
retorno = salvo com sucesso !!!

requisição via PUT(atualizar informações de rotas)
url base = localhost/rotas
exemplo  Json para atualização= { "id":"1", "origem":"A", "destino":"D", "distancia":"30" }
retorno = atualizado com sucesso !!!

requisição via DELETE(excluir informações de rotas)
url base = localhost/rotas
exemplo Json para atualização = { "id":"1", "origem":"A", "destino":"D", "distancia":"30" }
retorno = excluido com sucesso !!!





